package no.uib.inf101.terminal;

public interface Command {
    String run(String[] args);
    String getName();
  
    String[] args = new String[] { "foo", "bar" };
    Command command = null;
    String result = command.run(args);
}