package no.uib.inf101.terminal;

// UiB INF101 ShellLab - TextAnswers.java
//
// I denne filen skal du besvare spørsmålene under. Du kan sjekke
// svarene dine ved å kjøre testen TextAnswersTest.java.

public class TextAnswers {

  /**
   * Consider the following code:
   * CommandLineInterface cli = new DummyShell();
   *return (String q1);
   * What is the type of the variable cli?
   */
  static final String q1 = "CommandLineInterface"; {
    System.out.println (q1);
  }
  /**
   * Consider the following code:
   * CommandLineInterface cli = new DummyShell();
   *
   * In which class is the object cli refers to?
   */
  static final String q2 = "DummyShell"; {
    System.out.println(q2);
  }
   

  /**
   * Consider the following code:
   * CommandLineInterface cli = new DummyShell();
   *
   * true or false: CommandLineInterface is both a type and an interface.
   */
  static final Boolean q3 = true; {
    System.out.println(q3);
  }
  /**
   * Consider the following code:
   * CommandLineInterface cli = new DummyShell();
   *
   * true or false: DummyShell is both a class and a type.
   */
  static final Boolean q4 = true;{
    System.out.println(q4);
  }
}